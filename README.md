**ECO STORE**
A user story lays a crucial role in capturing and prioritizing features from user’s perspectives, they are essential elements of Agile methodologies like Scrum and Kanban that help developers build applications that actually solve problems and deliver value
From Agile principles, a user story helps developers to add functionality to meet a user’s need.
They typically follow a template like “AS a [user type], I want [to do something] so that [I achieve a certain goal]. Examples of user stories identified for the assignment;
As a user of ECO-STORE App, I want to be able to log into and ensure the security of my data. Since the user wants privacy of his data, we need a login page, registration page for new users and password recovery options. Email notifications will also be enabled. This will ensure only the authorized user has access to his data. The user can update account information once logged in.
As a user of the EC O-STORE App, I want to manage my account in such a way that I can edit, add content to my account page and also delete the content
Functional Requirements:
• Business Account:
o Businesses can create accounts with basic information (name, contact details, location, 
description, etc.).
o Businesses can upload logos, photos, and videos to showcase their offerings.
o Businesses can manage their menus, service listings, or appointment slots.
o Businesses can set pricing and availability for their offerings.
• Customer Functionality:
o Customers can browse businesses by category, location, or keywords.
o Customers can view detailed business profiles, menus, service descriptions, or 
appointment schedules.
o Customers can place orders for online delivery or pickup.
o Customers can book appointments for haircuts, massages, or fitness classes.
o Customers can leave reviews and ratings for businesses
• Admin Panel:
o Admin can manage user accounts (businesses and customers).
o Admin can monitor platform activity (orders, bookings, reviews).
o Admin can generate reports and analytics for businesses.
o Admin can manage platform settings and configurations.
Technical Requirements:
• Backend: Django framework for server-side development.
• Database: PostgreSQL or MySQL for storing user, business, and service data.
• Email API: SendGrid or Mailgun for order confirmations, booking reminders, etc.
• Cloud Hosting: Heroku, AWS, or Google Cloud Platform for web application hosting
