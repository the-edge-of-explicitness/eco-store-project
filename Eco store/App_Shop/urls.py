from django.urls import path
from App_Shop import views
from django.contrib.auth import views as auth_views
app_name = 'App_Shop'

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('add_product', views.add_product, name='add_product'),
    path('search', views.search, name='search'),
    path('checkout/', views.checkout, name='checkout'),
    path('orders/', views.orders, name='orders'),
    path('register_business', views.register_business, name='register_business'),
    path('product/<pk>/', views.ProductDetail.as_view(), name='product_detail'),
    path('about/', views.about, name='about_page'),
    path('my_businesses/', views.my_businesses, name='my_businesses'),
    path('contact/', views.contact, name='contact_page'),
    path('business_detail/<int:pk>/', views.business_detail, name='business_detail'),
    path('delete_product/', views.delete_product, name='delete_product'),
    path('product/<int:pk>/delete/', views.delete_product, name='delete_product'),
]