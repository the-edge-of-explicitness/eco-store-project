from django import forms
from .models import Product, Category,Business,DeliveryAddress

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['business','name', 'preview_text', 'detail_text', 'category', 'price', 'old_price', 'mainimage']
    name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter name of product',
        'class':'box'
    }))
    preview_text=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Preview text',
        'class':'box'
    }))
    detail_text=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Product Description',
        'class':'box'
    }))
   
    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        # Restrict category choices if needed (optional)
        self.fields['category'].queryset = Category.objects.all()  # Get all categories

class BusinessForm(forms.ModelForm):
    class Meta:
        model = Business
        fields = ['name','owner', 'business_type', 'registration_number', 'tax_id', 'city', 'country','logo']
    name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter Business name',
        'class':'box'
    }))
    owner=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your name',
        'class':'box'
    }))
    business_type=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter type of business',
        'class':'box'
    }))
    registration_number=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter Reg Number',
        'class':'box'
    }))
    tax_id=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter Tax ID',
        'class':'box'
    }))
    city=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter current city',
        'class':'box'
    }))
    country=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter country',
        'class':'box'
    }))
    
    


class DeliveryAddressForm(forms.ModelForm):
    class Meta:
        model=DeliveryAddress
        fields=['full_name','phone_number','company_name','address_line','city','country']
    full_name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your names',
        'class':'box'
    }))
    phone_number=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your tel number',
        'class':'box'
    }))
    company_name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter company name',
        'class':'box'
    }))
    address_line=forms.CharField(widget=forms.EmailInput(attrs={
        'placeholder':'Enter your address line',
        'class':'box'
    }))
    # address_line2=forms.CharField(widget=forms.EmailInput(attrs={
    #     'placeholder':'Enter address line 2',
    #     'class':'box'
    # }))
    city=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your current city',
        'class':'box'
    }))
    country=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your country',
        'class':'box'
    }))
    





