from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.

class Category(models.Model):
    title = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ('-created',)

class Business(models.Model):
    class Meta:
        verbose_name_plural='Businesses'

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255,default='')
    logo = models.ImageField(upload_to='businesses/logos', default='')
    owner=models.CharField(max_length=120,default='')
    business_type = models.CharField(max_length=100,default='')
    registration_number = models.CharField(max_length=50, blank=True)
    tax_id = models.CharField(max_length=50, blank=True)
    street_address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=100, blank=True)
    #division = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return self.name

class Product(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE, default='')
    mainimage = models.ImageField(upload_to='Products')
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='category')
    brand = models.CharField(max_length=100, default="Enter the brand")
    preview_text = models.TextField(max_length=200, verbose_name='Preview Text')
    detail_text = models.TextField(max_length=1000, verbose_name='Description')
    price = models.IntegerField()
    old_price = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-created',)

class Service(models.Model):
    business = models.ForeignKey(Business,on_delete=models.CASCADE,default='')
    name=models.CharField(max_length=120)
    image=models.ImageField(upload_to='services/images')
    description=models.TextField(default='')
    is_active=models.BooleanField(default=True)

    def __str__(self):
        return self.name







class DeliveryAddress(models.Model):
   
    full_name = models.CharField( max_length=255)
    phone_number = models.CharField( max_length=20)
    company_name = models.CharField( max_length=255, blank=True)
    address_line = models.CharField( max_length=255)
    # address_line2 = models.CharField( max_length=255, blank=True)
    city = models.CharField( max_length=100)
    country = models.CharField( max_length=100)

    class Meta:
        verbose_name = ("Delivery Address")
        verbose_name_plural = ("Delivery Addresses")

    def __str__(self):
        return f"{self.full_name} - {self.address_line1}, {self.city}"
class Order(models.Model):
    name = models.CharField(max_length=20, default="")

