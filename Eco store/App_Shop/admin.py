from django.contrib import admin
from App_Shop.models import *

# Register your models here.

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Business)
admin.site.register(DeliveryAddress)
admin.site.register(Service)
admin.site.register(Order)