# Generated by Django 5.0.3 on 2024-04-28 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App_Shop', '0016_remove_product_brand'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='brand',
            field=models.CharField(default='enter the brand', max_length=100),
        ),
    ]
