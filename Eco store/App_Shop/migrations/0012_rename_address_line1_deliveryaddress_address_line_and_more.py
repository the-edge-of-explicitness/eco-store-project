# Generated by Django 5.0.3 on 2024-04-11 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App_Shop', '0011_delete_address_alter_deliveryaddress_city'),
    ]

    operations = [
        migrations.RenameField(
            model_name='deliveryaddress',
            old_name='address_line1',
            new_name='address_line',
        ),
        migrations.RemoveField(
            model_name='business',
            name='division',
        ),
        migrations.RemoveField(
            model_name='deliveryaddress',
            name='address_line2',
        ),
        migrations.AddField(
            model_name='business',
            name='logo',
            field=models.ImageField(default='', upload_to='businesses/logos'),
        ),
    ]
