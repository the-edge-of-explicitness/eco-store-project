# Generated by Django 5.0.3 on 2024-04-11 10:19

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App_Shop', '0012_rename_address_line1_deliveryaddress_address_line_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='business',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='App_Shop.business'),
        ),
        migrations.CreateModel(
            name='Services',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('image', models.ImageField(upload_to='services/images')),
                ('description', models.TextField(default='')),
                ('is_active', models.BooleanField(default=True)),
                ('business', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='App_Shop.business')),
            ],
        ),
    ]
