from django.forms import ModelForm
from App_Login.models import User, Profile
from django import forms

from django.contrib.auth.forms import UserCreationForm

#UserProfileForm
class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        exclude = ('user',)
        fields = ('full_name', 'address_1', 'city', 'country', 'phone')
    full_name=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your full name',
        'class':'box'
    }))
    address_1=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your address',
        'class':'box'
    }))
    city=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your current city',
        'class':'box'
    }))
    country=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your country',
        'class':'box'
    }))
    phone=forms.CharField(widget=forms.TextInput(attrs={
        'placeholder':'Enter your mobile phone number',
        'class':'box'
    }))

#SignUpForm
class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email','password1','password2',)
    email=forms.CharField(widget=forms.EmailInput(attrs={
        'placeholder':'Enter your email',
        'class':'box'
    }))
    password1=forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder':'Enter your password',
        'class':'box'
    }))
    password2=forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder':'Confirm your password',
        'class':'box'
    }))
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].help_text = "" 
   